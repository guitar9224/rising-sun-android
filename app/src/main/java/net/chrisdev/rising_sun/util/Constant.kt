package net.chrisdev.rising_sun.util

class Constant {
    companion object {
        const val UUID = "uuid"
        const val CHANNEL_ID = "rising-sun-channel"
        const val DEVICE_TOKEN_LIST = "DeviceTokenList"
    }
}