package net.chrisdev.rising_sun.service

import android.app.NotificationManager
import android.content.Context
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import net.chrisdev.rising_sun.R
import net.chrisdev.rising_sun.util.Constant

class MSFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = MSFirebaseMessagingService::class.java.simpleName

    override fun onCreate() {
        super.onCreate()
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        Log.d(TAG, "신규 토큰 발급 : token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val nTitle = remoteMessage.notification?.title
        val nBody = remoteMessage.notification?.body

        if (nTitle.isNullOrEmpty() && nBody.isNullOrEmpty()) {
            val dTitle = remoteMessage.data["title"].orEmpty()
            val dBody = remoteMessage.data["body"].orEmpty()

            showNotification(dTitle, dBody)
        }
    }

    private fun showNotification(title: String, content: String) {
        val builder = NotificationCompat.Builder(this, Constant.CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle(title)
            .setContentText(content)
            .setPriority(NotificationCompat.PRIORITY_HIGH)

        val notiManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notiManager.notify(1234, builder.build())
    }
}