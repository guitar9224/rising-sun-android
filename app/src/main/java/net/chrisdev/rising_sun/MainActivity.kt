package net.chrisdev.rising_sun

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import net.chrisdev.rising_sun.util.Constant
import java.util.*


class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.simpleName

    private lateinit var mFirebaseDB: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createNotificationChannel()

        mFirebaseDB = FirebaseFirestore.getInstance()

        subscribeToken()
        getTokenList()

        btn_notification.setOnClickListener {
            showNotify()
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(Constant.CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun showNotify() {
        val builder = NotificationCompat.Builder(this, Constant.CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("MyTitle")
            .setContentText("MyTest")
            .setPriority(NotificationCompat.PRIORITY_MAX)

        val notiManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notiManager.notify(1234, builder.build())
    }

    private fun subscribeToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            putToken(it.token)
        }.addOnFailureListener {
            setText("토큰값 : 토큰 발급 실패 : ${it.message}")
        }
    }

    private fun putToken(token: String) {
        if (token.isEmpty()) {
            return
        }

        mFirebaseDB.collection(Constant.DEVICE_TOKEN_LIST).document(token)
            .set(hashMapOf(Constant.UUID to UUID.randomUUID()), SetOptions.merge())
            .addOnSuccessListener {
                setText("업로드 완료 : $token")
            }.addOnFailureListener { throwable ->
                Log.e(TAG, throwable.message ?: "")
            }
    }

    private fun getTokenList() {
        mFirebaseDB.collection(Constant.DEVICE_TOKEN_LIST).get()
            .addOnSuccessListener {
                setText(it.documents)
            }.addOnFailureListener {
                setText("토큰 목록 호출 실패 : ${it.message}")
            }
    }

    @SuppressLint("SetTextI18n")
    private fun setText(value: String) {
        val origin = txt_content.text.toString()
        txt_content.text = "$origin \n$value \n\n"
    }

    private fun setText(list: List<DocumentSnapshot>) {
        if (list.isEmpty()) {
            return
        }

        list.forEach {
            val token = it.id
            val uuid = it.get(Constant.UUID)

            setText("서버에서 가져온 값 UUID : $uuid \n\n token: $token")
        }
    }
}